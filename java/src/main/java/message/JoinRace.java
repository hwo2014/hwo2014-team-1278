/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message;

/**
 *
 * @author Corne
 */
public class JoinRace extends BaseMessage {
    public static final String JOINRACE = "joinRace";
    
    private final Join botId;
    private final String trackName;
    private final int carCount;

    public JoinRace(Join botId, String trackName, int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.carCount = carCount;
    }

    
    
    @Override
    public String msgType() {
        return JOINRACE;
    }
}
