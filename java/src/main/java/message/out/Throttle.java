/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.out;

import message.BaseMessage;

/**
 *
 * @author Corne
 */
public class Throttle extends BaseMessage {
    private final double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    public Object msgData() {
        return value;
    }

    @Override
    public String msgType() {
        return "throttle";
    }
}
