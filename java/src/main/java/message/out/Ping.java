/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.out;

import message.BaseMessage;

/**
 *
 * @author Corne
 */
public class Ping extends BaseMessage {
    
    @Override
    public String msgType() {
        return "ping";
    }
}
