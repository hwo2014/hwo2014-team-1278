/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in.match;

/**
 *
 * @author Corne
 */
public class Point { //for example starting point, game init
    private final Position position;
    private final double angle;

    public Point(Position position, double angle) {
        this.position = position;
        this.angle = angle;
    }
}
