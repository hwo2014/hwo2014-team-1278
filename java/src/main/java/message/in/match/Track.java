/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in.match;

import java.util.ArrayList;

/**
 *
 * @author Corne
 */
public class Track {
    private final String id;
    private final String name;
    private final ArrayList<TrackPiece> pieces;
    private final ArrayList<Lane> lanes;
    private final Point startingPoint;

    public Track(String id, String name, ArrayList<TrackPiece> pieces, ArrayList<Lane> lanes, Point startingPoint) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;
        this.startingPoint = startingPoint;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<TrackPiece> getPieces() {
        return pieces;
    }

    public ArrayList<Lane> getLanes() {
        return lanes;
    }

    public Point getStartingPoint() {
        return startingPoint;
    }
    
    
}
