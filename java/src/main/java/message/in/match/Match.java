/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in.match;

import message.IMessage;

/**
 *
 * @author Corne
 */
public class Match implements IMessage {
    
    public static final String GAMEINIT = "gameInit";
    
    private final Race race;

    public Match(Race race) {
        this.race = race;   
    }

    public Race getRace() {
        return race;
    }
    
    @Override
    public String getType() {
        return GAMEINIT;
    }
}
