/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in.match;

import java.util.ArrayList;

/**
 *
 * @author Corne
 */
public class Race {
    private final Track track;
    private final ArrayList<Car> cars;
    private final Session raceSession;

    public Race(Track track, ArrayList<Car> cars, Session raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }

    public Track getTrack() {
        return track;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public Session getRaceSession() {
        return raceSession;
    }
    
    
}
