/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in.match;

/**
 *
 * @author Corne
 */
public class Session {
    private final int laps;
    private final int maxLapTimeMs;
    private final Boolean quickRace;
    
    public Session(int laps, int maxlapTimeMs, Boolean quickRace){
        this.laps = laps;
        this.maxLapTimeMs = maxlapTimeMs;
        this.quickRace = quickRace;
    }
}
