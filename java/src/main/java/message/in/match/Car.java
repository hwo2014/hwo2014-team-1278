/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in.match;

import message.in.CarId;

/**
 *
 * @author Corne
 */
public class Car {
    private final CarId id;
    private final CarDimensions dimensions;

    public Car(CarId id, CarDimensions dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }
}
