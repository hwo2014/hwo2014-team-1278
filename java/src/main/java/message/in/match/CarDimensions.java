/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in.match;

/**
 *
 * @author Corne
 */
public class CarDimensions {
    private final double length;
    private final double width;
    private final double guidFlagPosition;

    public CarDimensions(double length, double width, double guidFlagPosition) {
        this.length = length;
        this.width = width;
        this.guidFlagPosition = guidFlagPosition;
    }
    
    
}
