/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in.match;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Corne
 */
public class TrackPiece {
    //todo (different types of pieces)
    private final double length;
    @SerializedName("switch")
    private final Boolean flip;
    private final int radius;
    private final double angle;

    public TrackPiece(double length, Boolean flip, int radius, double angle) {
        this.length = length;
        this.flip = flip;
        this.radius = radius;
        this.angle = angle;
    }

    public double getLength() {
        return length;
    }

    public Boolean isFlip() {
        return flip;
    }

    public int getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }

    
    
    
}
