/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in;

/**
 *
 * @author Corne
 */
public class CarPosition {
    private final CarId id;
    private final double angle;
    private final PiecePosition piecePosition;

    public CarPosition(CarId id, double angle, PiecePosition piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }

    public CarId getId() {
        return id;
    }

    public double getAngle() {
        return angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }
    
    
    
    
}
