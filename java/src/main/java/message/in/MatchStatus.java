/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import message.IMessage;

/**
 *
 * @author Corne
 */
public class MatchStatus implements IMessage {
    
    public static final String CARPOSITIONS = "carPositions";
    
    @SerializedName("data")
    private final ArrayList<CarPosition> carPositions;
    private final String gameId;
    private final int gameTick;

    public MatchStatus(ArrayList<CarPosition> carPositions, String gameId, int gameTick) {
        this.carPositions = carPositions;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }

    public String getGameId() {
        return gameId;
    }

    public int getGameTick() {
        return gameTick;
    }
    
    public CarPosition getCarPosition(CarId id){
//        return carPositions.parallelStream()
//                .filter(c -> c.getId().equals(id))
//                .findFirst().get();
        for(CarPosition position : carPositions){
            if(position.getId().equals(id)){
                return position;
            }
        }
        return null;
    }
    
    @Override
    public String getType() {
        return CARPOSITIONS;
    }
    
}
