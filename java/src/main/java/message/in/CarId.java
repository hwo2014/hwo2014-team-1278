/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in;

import java.util.Objects;
import message.BaseMessage;
import message.IMessage;

/**
 *
 * @author Corne
 */
public class CarId extends BaseMessage implements IMessage {
    
    public static final String YOURCAR = "yourCar";
    
    private final String name;
    private final String color;
    
    public CarId(final String name, final String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public String msgType() {
        return YOURCAR;
    }

    @Override
    public String getType() {
        return YOURCAR;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.color);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CarId other = (CarId) obj;
        return Objects.equals(this.color, other.color);
    }

    

    
    
}
