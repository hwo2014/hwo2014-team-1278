/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message.in;

import message.in.match.Lane;

/**
 *
 * @author Corne
 */
public class PiecePosition {
    private final int pieceIndex;
    private final double inPieceDistance;
    private final Lane lane;
    private final int lap;

    public PiecePosition(int pieceIndex, double inPieceDistance, Lane lane, int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public Lane getLane() {
        return lane;
    }

    public int getLap() {
        return lap;
    }
    
    
}
