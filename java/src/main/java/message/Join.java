/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message;

/**
 *
 * Message that bot will send to join the game, 
 * and we expect to return from the server to confirm the join
 * @author Corne
 */
public class Join extends BaseMessage implements IMessage {
    
    public static final String JOIN = "join";
    
    private final String name;
    private final String key;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }
    
    @Override
    public String getType() {
        return JOIN;
    }

    @Override
    public String msgType() {
        return JOIN;
    }
}
