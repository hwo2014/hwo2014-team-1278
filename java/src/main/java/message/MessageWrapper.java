/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message;

/**
 *
 * @author Corne
 */
public class MessageWrapper {
    public final String msgType;
    public final Object data;

    public MessageWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MessageWrapper(final BaseMessage sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}
