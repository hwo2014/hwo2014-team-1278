/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message;

/**
 *
 * @author Corne
 */
public enum Message {
    JOIN("join");
    
    private final String message;
    
    private Message(final String message){
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message;
    }
    
}
