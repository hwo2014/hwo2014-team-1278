/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package message;

import com.google.gson.Gson;

/**
 *
 * @author Corne
 */
public abstract class BaseMessage { 
    public String toJson() {
        return new Gson().toJson(new MessageWrapper(this));
    }

    public Object msgData() {
        return this;
    }

    public abstract String msgType();
}
