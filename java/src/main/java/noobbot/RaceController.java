/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot;

import java.util.ArrayList;
import java.util.List;
import message.in.CarId;
import message.in.CarPosition;
import message.in.MatchStatus;
import message.in.match.Match;
import message.in.match.TrackPiece;
import message.out.Throttle;

/**
 *
 * @author Corne
 */
public class RaceController {

    private Match match;
    private CarId carId;
    private MatchStatus currentStatus;

    //todo pass match and car id in constructor?
    public RaceController() {

    }

    public void updateCurrentPosition(MatchStatus update) {
        if (currentStatus == null || update.getGameTick() > currentStatus.getGameTick()) {
            this.currentStatus = update;
        }
    }

    private int getNextIndex(List<TrackPiece> pieces, int currentIndex) {
        if (currentIndex == pieces.size() - 1) {
            return 0;
        } else {
            return currentIndex + 1;
        }
    }

    // todo clean code + better logic :D
//    public Throttle calculateBestThrottle() {
//        CarPosition position = currentStatus.getCarPosition(carId);
//        int currentIndex = position.getPiecePosition().getPieceIndex();
//        ArrayList<TrackPiece> pieces = match.getRace().getTrack().getPieces();
//        int nextPieceIndex = getNextIndex(pieces, currentIndex);
//        TrackPiece nextPiece = pieces.get(nextPieceIndex);
//        int nextPieceIndex2 = getNextIndex(pieces, nextPieceIndex);
//        TrackPiece nextPiece2 = pieces.get(nextPieceIndex2);
//        
//        // we base action on following pieces, because before we send a message back the car is probaly arround the next piece
//        //for good logic we gonna need to include the radius to calculate the corners
//        
//        System.out.println("angle: " + currentStatus.getCarPosition(carId).getAngle());
//        if(nextPiece2.getLength() > 0 && nextPiece.getLength() > 0){ // double straight
//            return new Throttle(1.0);
//        } else if((nextPiece2.getAngle() > -45 && nextPiece2.getAngle() < 45) && nextPiece.getLength() > 0){ // slow down for next
//            return new Throttle(0.8);
//        } else if(nextPiece.getLength() > 0 && (nextPiece.getAngle() > -45 && nextPiece.getAngle() < 45)) { // speed up for straight
//            //System.out.println("Radius: " + nextPiece.getRadius() + " Angle2: " + nextPiece.getAngle());
//            return new Throttle(0.8);
//        }
//        
//        //System.out.println("Angle 1: " + nextPiece.getAngle() + " Angle2: " + nextPiece2.getAngle());
//        return new Throttle(0.5);
//    }
    public Throttle calculateBestThrottle() {
        CarPosition position = currentStatus.getCarPosition(carId);
        int currentIndex = position.getPiecePosition().getPieceIndex();
        ArrayList<TrackPiece> pieces = match.getRace().getTrack().getPieces();
        int nextPieceIndex = getNextIndex(pieces, currentIndex);
        TrackPiece currentPiece = pieces.get(currentIndex);
        TrackPiece nextPiece = pieces.get(nextPieceIndex);

        double carAngle = position.getAngle();
        double inPieceDistance = position.getPiecePosition().getInPieceDistance();


        if (currentPiece.getAngle() != 0 && nextPiece.getAngle() == 0 && inPieceDistance > 50.0
                && carAngle < 40 && carAngle > -40) { // speed up for next straight piece
            return new Throttle(1.0);
        } else if (currentPiece.getAngle() != 0 && nextPiece.getAngle() == 0 && inPieceDistance > 25.0
                && carAngle < 25 && carAngle > -25) { // speed up for next straight piece
            return new Throttle(1.0);
        } else if (currentPiece.getAngle() != 0 && nextPiece.getAngle() == 0 && inPieceDistance > 20.0
                && carAngle < 20 && carAngle > -20) { // speed up for next straight piece
            return new Throttle(1.0);
        }
        else if (currentPiece.getAngle() != 0 && nextPiece.getAngle() == 0 && inPieceDistance > 11.0
                && carAngle < 10 && carAngle > -10) { // speed up for next straight piece
            return new Throttle(0.9);
        } else if (currentPiece.getAngle() != 0 && nextPiece.getAngle() == 0 && inPieceDistance > 5.0
                && carAngle < 5 && carAngle > -5) { // speed up for next straight piece
            return new Throttle(0.8);
        }
        return calculateBasedOnCarAngle();
    }

    /**
     * Method that will make sure we slow down in corners if we go to fast, so
     * we never crash :D
     *
     * @return
     */
    public Throttle calculateBasedOnCarAngle() {
        double carAngle = currentStatus.getCarPosition(carId).getAngle();
        if (carAngle == 0) {
            return new Throttle(1.0);
        } else if (carAngle < 10 && carAngle > -10) {
            return new Throttle(0.7);
        } else if (carAngle < 14 && carAngle > -14) {
            return new Throttle(0.6);
        } else if (carAngle < 18 && carAngle > -18) {
            return new Throttle(0.5);
        } else if (carAngle < 22 && carAngle > -22) {
            return new Throttle(0.4);
        } else if (carAngle < 27 && carAngle > -27) {
            return new Throttle(0.3);
        } else if (carAngle < 31 && carAngle > -31) {
            return new Throttle(0.2);
        } else if (carAngle < 35 && carAngle > -35) {
            return new Throttle(0.1);
        } else if (carAngle < 40 && carAngle > -40) {
            return new Throttle(0.1);
        } else {
            System.out.println("PRAY TO THE MIGHTY SPAGHETTI MONSTER THAT WE WON'T CRASH " + carAngle);
            return new Throttle(0.1);
        }
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public CarId getCarId() {
        return carId;
    }

    public void setCarId(CarId carId) {
        this.carId = carId;
    }

}
