package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import marshaller.MessageMarshaller;
import message.Join;
import message.MessageWrapper;
import message.BaseMessage;
import message.JoinRace;
import message.in.CarId;
import message.in.MatchStatus;
import message.in.match.Match;
import message.out.Ping;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        Main main = new Main(reader, writer, new Join(botName, botKey));
        //Main main = new Main(reader, writer, new JoinRace(new Join(botName, botKey), "germany", 1));

    }

    final Gson gson = new Gson();
    private final PrintWriter writer;
    private RaceController controller;

    public Main(final BufferedReader reader, final PrintWriter writer, final BaseMessage join) throws IOException {
        this.writer = writer;
        this.controller = new RaceController();
        
        MessageMarshaller marshaller = new MessageMarshaller();
        
        String line;

        send(join);

        while((line = reader.readLine()) != null) {
            final MessageWrapper msgFromServer = gson.fromJson(line, MessageWrapper.class);
            switch (msgFromServer.msgType) {
                case MatchStatus.CARPOSITIONS:
                    //System.out.println("update");
                    //data is array positions, so exception on the rule of the unmarshaller
                    //MatchStatus status = marshaller.jsonToMessage(line, MatchStatus.class);
                    MatchStatus status = gson.fromJson(line, MatchStatus.class);
                    controller.updateCurrentPosition(status);
                    send(controller.calculateBestThrottle());
                    break;
                case Join.JOIN:
                    Join test = marshaller.jsonToMessage(line, Join.class);
                    System.out.println("Joined: " + test.getKey());
                    break;
                case CarId.YOURCAR:
                    System.out.println("Your car");
                    CarId car = marshaller.jsonToMessage(line, CarId.class);
                    this.controller.setCarId(car);
                case Match.GAMEINIT:
                    System.out.println("Race init");
                    Match match = marshaller.jsonToMessage(line, Match.class);
                    this.controller.setMatch(match);
                    break;
                case "gameEnd":
                    System.out.println("Race end");
                    break;
                case "gameStart":
                    System.out.println("Race start");
                    break;
                default:
                    System.out.println("UNKNOWN MESSAGE: " + line);
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(final BaseMessage msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}