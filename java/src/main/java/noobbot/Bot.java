/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot;

import java.io.PrintWriter;
import message.BaseMessage;

/**
 *
 * @author Corne
 */
public abstract class Bot {

    private final PrintWriter writer;

    public Bot(PrintWriter writer) {
        this.writer = writer;
    }

    public void join() {
        //this.
    }
    
    private void send(final BaseMessage msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}
