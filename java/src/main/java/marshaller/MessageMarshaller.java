/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package marshaller;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import message.IMessage;

/**
 *
 * @author Corne
 */
public class MessageMarshaller {
    
    private static final String DATA_CONTAINER = "data";
    
    public <T extends IMessage> T jsonToMessage(String json, Class<T> classOfT) {
        JsonParser parser = new JsonParser();        
        JsonObject topObject = parser.parse(json).getAsJsonObject();
        JsonElement element = topObject.get(DATA_CONTAINER);
        
        Gson gson = new Gson();
        return gson.fromJson(element, classOfT);
    }
    
}
